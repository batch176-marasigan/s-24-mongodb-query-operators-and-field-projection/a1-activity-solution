//add course collection
db.courses.insertMany([
    {
 
        "name": "HTML Basics",
        "price": 20000,
        "isActive": true,
        "instructor": "Sir Marty"
       
    },

    {
     
        "name": "CSS 101 + Flexbox ",
        "price": 21000,
        "isActive": true,
        "instructor": "Sir Marty"
       
     },

 {
       
       
        "name": "Javascript 101",
        "price": 32000,
        "isActive": true,
        "instructor": "Ma'am Joy"
       
     },


{

       
        "name": "Git 101, IDE and CLI",
        "price": 19000,
        "isActive": false,
        "instructor": "Ma'am Joy"
       

     },
 

  {
       
       
        "name": "React.Js 101",
        "price": 25000,
        "isActive": true,
        "instructor": "Ma'am Miah"
       
     }    

])


// find courses whose instructor is "Sir Marty" and is priced >=20000 
// show only its name and price

db.courses.find({
    
    $and:[
       {instructor:"Sir Marty"},
       {price:{$gte:20000}}
        ]},
        {"_id":0,"name":1,"price":1} 
     )

// find instructor Ma'am Joy and is inActive
// - show only its name and price 

db.courses.find({
    
    $and:[
       {instructor:"Ma'am Joy"},
       {isActive:false}
        ]},
        {"_id":0,"name":1,"price":1} 
     )

//find course with letter "r" its name and price <=25000
   db.courses.find(
     {$and:[
       {price:{$lte:25000}},
       {name:{$regex:'r',$options:'$i'}},
       ]}
     //,{"_id":0,"name":1,"price":1} 
    )


//update all courses with price<21000 to inActive

db.courses.updateMany( 
 {price:{$lt:21000}},
    {$set: {isActive:false}}
)



//delete all courses with price>=25000
db.courses.deleteMany( {price:{$gte:25000}})

